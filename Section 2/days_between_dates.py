# define days for each month in a non leap year
regularDaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
# define days for each month in a leap year
leapDaysInMonth = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
# define days per year
leapYearDays = 366
regularYearDays = 365

# define function to determine if year is a leap year


def isLeapYear(year):
    if year % 4  == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else: return False
        else:
            return True
    return False


# define function to calculate days between two dates

def daysBetweenDates(year1, month1, day1, year2, month2, day2):
    startYear = year1
    endYear = year2
    yearDays = 0
    year1Days = 0
    year2Days = 0
    year2Remainder = 0
    # find total days between years
    while startYear <= endYear:
        if isLeapYear(startYear) is True:
            yearDays = yearDays + leapYearDays
        else:
            yearDays = yearDays + regularYearDays
        startYear = startYear + 1
    # find days consumed in year1
    i1 = 0
    while i1 < month1 - 1:
        if isLeapYear(year1) is True:
            year1Days = year1Days + leapDaysInMonth[i1]
            i1 = i1 + 1
        else:
            year1Days = year1Days + regularDaysInMonth[i1]
            i1 = i1 + 1
    year1Days = year1Days + day1
    # find days consumed in year2
    i2 = month2
    while i2 < 12:
        if isLeapYear(year2) is True:
            year2Days = year2Days + leapDaysInMonth[i2]
            i2 = i2 + 1
        else:
            year2Days = year2Days + regularDaysInMonth[i2]
            i2 = i2 + 1
    if isLeapYear(year2) is True:
        year2Remainder = leapDaysInMonth[(month2 - 1)] - day2
    else:
        year2Remainder = regularDaysInMonth[(month2 - 1)] - day2
    return yearDays - year1Days - year2Days - year2Remainder

print(daysBetweenDates(2012, 1, 1, 2012, 2, 28))
