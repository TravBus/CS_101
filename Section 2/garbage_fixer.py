def fix_machine(debris, product):
    p_pos = 0
    word = ''
    while debris.find(product[p_pos:p_pos+1]) != -1:
        d_pos = debris.find(product[p_pos:p_pos+1])
        word = word + debris[d_pos]
        p_pos = p_pos + 1
        if debris.find(product[p_pos:p_pos+1]) == -1:
            print "Give me something that's not useless next time."
        if word == product:
            print word
            break
### TEST CASES ###
print "Test case 1: ", fix_machine('UdaciousUdacitee', 'Udacity') == "Give me something that's not useless next time."
print "Test case 2: ", fix_machine('buy me dat Unicorn', 'Udacity') == 'Udacity'
print "Test case 3: ", fix_machine('AEIOU and sometimes y... c', 'Udacity') == 'Udacity'
print "Test case 4: ", fix_machine('wsx0-=mttrhix', 't-shirt') == 't-shirt'
