# define abicus structure

al = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
root = '|00000'

#         |00000*****   |     row factor 1,000,000,000
#         |00000*****   |     row factor 100,000,000
#         |00000*****   |     row factor 10,000,000
#         |00000*****   |     row factor 1,000,000
#         |00000*****   |     row factor 100,000
#         |00000*****   |     row factor 10,000
#         |00000*****   |     row factor 1,000
#         |00000****   *|     row factor 100
#         |00000***   **|     row factor 10
#         |00000**   ***|     row factor 1

# test number

n = 2234567800


def levels(some_number):
    varnum = some_number
    if len(str(varnum)) == al[0]:
        divisor = varnum/1000000000
        if divisor == 1:
            return root + '****   *|'
        if divisor == 2:
            return root + '***   **|'
        if divisor == 3:
            return root + '**   ***|'
        if divisor == 4:
            return root + '*   ****|'
        if divisor == 5:
            return root + '   *****|'
        varnum = varnum - 1000000000 * divisor
    if len(str(some_number)) == al[1]:
        divisor = varnum / 100000000
        if divisor == 1:
            return root + '****   *|'
        if divisor == 2:
            return root + '***   **|'
        if divisor == 3:
            return root + '**   ***|'
        if divisor == 4:
            return root + '*   ****|'
        if divisor == 5:
            return root + '   *****|'
        varnum = varnum - 100000000 * divisor


print(levels(n))
