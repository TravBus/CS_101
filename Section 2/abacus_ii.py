# define abicus structure

def abacus_prints(div):
    if div == 1:
        return '|00000****   *|'
    if div == 2:
        return '|00000***   **|'
    if div == 3:
        return '|00000**   ***|'
    if div == 4:
        return '|00000*   ****|'
    if div == 5:
        return '|00000   *****|'
    if div == 6:
        return '|0000   0*****|'
    if div == 7:
        return '|000   00*****|'
    if div == 8:
        return '|00   000*****|'
    if div == 9:
        return '|0   000******|'
    if div == 0:
        return '|00000*****   |'

def print_abacus(n):
# find total rows
    rownum = 10 - len(str(n))
# print unassigned
    ua = rownum
    while ua > 0:
        print abacus_prints(0)
        ua = ua - 1
    i = 0
    while i <= str(n).find(str(n)[-1]):
        print abacus_prints(int(str((n))[i]))
        i = i + 1

print_abacus(134)
