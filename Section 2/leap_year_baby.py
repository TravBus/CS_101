#If the year is evenly divisible by 4, go to step 2. Otherwise, go to step 5.
#If the year is evenly divisible by 100, go to step 3. Otherwise, go to step 4.
#If the year is evenly divisible by 400, go to step 4. Otherwise, go to step 5.
#The year is a leap year (it has 366 days).
#he year is not a leap year (it has 365 days).


def is_leap_baby(day,month,year):
    if (year % 4  == 0 and day == 29 and month == 2):
        if (year % 100 == 0 and day == 29 and month == 2):
            if (year % 400 == 0 and day == 29 and month == 2):
                return True
            else: return False
        else:
            return True
    return False

#    output(is_leap_baby(19, 6, 1978), 'Garfield')
