# zebra >> "Try to ride a zebra!"
# cheetah >> If you are faster than a cheetah: "Run!"
#            If you are not: "Stay calm and wait!".
#            The speed of a cheetah is 115 km/h.
# anything else >> "Introduce yourself!"

# Define a procedure, jungle_animal,
# that takes as input a string and a number,
# an animal and your speed (in km/h),
# and prints out what to do.

def jungle_animal(animal, my_speed):
    if animal == 'cheetah':
        if my_speed < 116:
            print "Stay calm and wait!"
        else:
            print "Run!"
    if animal == "zebra":
        print "Try to ride a zebra!"
    if (animal != 'cheetah' and animal != 'zebra'):
        print "Introduce yourself!"

jungle_animal('dog',10)
