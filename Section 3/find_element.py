# Define a procedure, find_element,
# that takes as its inputs a list
# and a value of any type, and
# returns the index of the first
# element in the input list that
# matches the value.

# If there is no matching element,
# return -1.

def find_element(lists, target):
    i = 0
    for e in lists:
        if lists[i] == target:
            return i
        else:
            i = i + 1
    return -1


print find_element([1,2,3],3)
#>>> 2

# print find_element(['alpha','beta'],'gamma')
#>>> -1

#print find_element(['alpha','beta', 'gamma'],'gamma')


----- rewrite after learning about list and in

def find_element(alist, target):
    if target in alist:
        return alist.index(target)
    else:
        return -1
