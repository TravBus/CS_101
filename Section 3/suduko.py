# THREE GOLD STARS

# Sudoku [http://en.wikipedia.org/wiki/Sudoku]
# is a logic puzzle where a game
# is defined by a partially filled
# 9 x 9 square of digits where each square
# contains one of the digits 1,2,3,4,5,6,7,8,9.
# For this question we will generalize
# and simplify the game.

# Define a procedure, check_sudoku,
# that takes as input a square list
# of lists representing an n x n
# sudoku puzzle solution and returns the boolean
# True if the input is a valid
# sudoku square and returns the boolean False
# otherwise.

# A valid sudoku square satisfies these
# two properties:

#   1. Each column of the square contains
#       each of the whole numbers from 1 to n exactly once.

#   2. Each row of the square contains each
#       of the whole numbers from 1 to n exactly once.

# You may assume the the input is square and contains at
# least one row and column.

correct = [[1,2,3],
           [2,3,1],
           [3,1,2]]

incorrect = [[1,2,3,4],
             [2,3,1,3],
             [3,1,2,3],
             [4,4,4,4]]

incorrect2 = [[1,2,3,4],
             [2,3,1,4],
             [4,1,2,3],
             [3,4,1,2]]

incorrect3 = [[1,2,3,4,5],
              [2,3,1,5,6],
              [4,5,2,1,3],
              [3,4,5,2,1],
              [5,6,4,3,2]]

incorrect4 = [['a','b','c'],
              ['b','c','a'],
              ['c','a','b']]

incorrect5 = [ [1, 1.5],
               [1.5, 1]]

test1 = [[1,2,4], [2,4,1], [4,1,2]]



def soduko_row(myList):
    newList = []
    i = 0
    # while loop to keep track of myList index
    while i <= len(myList[0]) - 1:
        # reset list when new list is advanced
        newList = []
        for e in myList[i]:
            # check if e is integer and not a duplicate
            if isinstance(e, int) and e not in newList:
                newList.append(e)
            else:
                return False
        # increase index by 1
        if 1 in newList:
            if 0 in newList:
                return False
            i = i + 1
        else:
            return False
    return True


def soduko_column(myList):
    i = 0
    newList = []
    while i < len(myList):
        newList = []
        for e in myList:
            if isinstance(e[i], int):
                if e[i] not in newList and e[i] <= len(myList):
                    newList.append(e[i])
                else:
                    return False
        # increase index by 1
        if 1 in newList:
            if 0 in newList:
                return False
            i = i + 1
        else:
            return False
    return True


def check_sudoku(myList):
    if soduko_row(myList):
        if soduko_column(myList):
            return True
        else:
            return False
    else:
        return False
        
#print check_sudoku(testcase)
print sudoku_row(test1)
#>>> False

#print check_sudoku(correct)
#>>> True

#print check_sudoku(incorrect2)
#>>> False

#print check_sudoku(incorrect3)
#>>> False

#print check_sudoku(incorrect4)
#>>> False

#print check_sudoku(incorrect5)
#>>> False
