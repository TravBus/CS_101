testcase = [[1,2,3,4],
            [2,4,1,3],
            [3,1,4,2],
            [4,3,2,1]]

def soduko_row(myList):
    newList = []
    i = 0
    # while loop to keep track of myList index
    while i <= len(myList[0]) - 1:
        # reset list when new list is advanced
        newList = []
        for e in myList[i]:
            # check if e is integer and not a duplicate
            if isinstance(e, int) and e not in newList:
                newList.append(e)
            else:
                return False
        # increase index by 1
        if 1 in newList:
            i = i + 1
        else:
            return False
    return newList

#print len(testcase[0])
print(soduko_row(testcase))
